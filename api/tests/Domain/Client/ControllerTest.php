<?php
  namespace Domain\Client;
  use Domain\User\User;

  /**
   *
   */
  class ControllerTest extends \TestCase
  {

    public function testCreate()
    {
      //sets
      $headers=$this->getHeaders();
    //  dd($headers);

      $name='Alcione Morais1';
        $data=[
        'name'=>$name,
      ];
      $this->post('client',$data, $headers);
      $this->seeStatusCode(200);
      $this->seejson([
        'name'=>$name,
      ]);
      $this->seeInDatabase('clients',[
        'name'=>$name,
      ]);
    }

    public function testCreateWithCpfAndBirthdate()
    {
      //sets
      $headers=$this->getHeaders();
    //  dd($headers);

      $name='Alcione Morais2';
      $cpf='36069161599';
      $birthdate='2016-09-11';
      $data=[
        'name'=>$name,
        'cpf'=>$cpf,
        'birthdate'=>$birthdate,
      ];
      $this->post('client', $data, $headers);
     //dd($this->response->getContent());
      $this->seeStatusCode(200);
      $this->seejson([
        'name'=>$name,
        'cpf'=>$cpf,
        'birthdate'=>$birthdate,
      ]);
      $this->seeInDatabase('clients',[
        'name'=>$name,
        'cpf'=>$cpf,
        'birthdate'=>$birthdate,
      ]);
    }

    public function testCreateWithOnlyCpf()
    {
      //sets
      factory(Client::class)->create([
        'cpf'=>'82781664049',
      ]);
      $headers=$this->getHeaders();
    //  dd($headers);

      $name='Alcione Morais4';
      $cpf='82781664049';
      $data=[
        'name'=>$name,
        'cpf'=>$cpf,
      ];
      $this->post('client', $data, $headers);
    //  dd($this->response->getContent());
      $this->seeStatusCode(422);
    }

    public function testUpdate()
    {
    //sets
    $client=factory(Client::class)->create([
      'cpf'=>'82781664049',
    ]);
    $headers=$this->getHeaders();

    $name='Alcione Morais5';
    $cpf='82781664049';
    $birthdate='2016-09-11';
    $data=[
      'name'=>$name,
      'cpf'=>$cpf,
      'birthdate'=>$birthdate,
    ];
    $this->put('client/'.$client->id, $data, $headers);
    //  dd($this->response->getContent());
    $this->seeStatusCode(200);
    $this->seejson([
      'name'=>$name,
      'cpf'=>$cpf,
      //'birthdate'=>$birthdate,
    ]);
    $this->seeInDatabase('clients',[
      'name'=>$name,
      'cpf'=>$cpf,
      //'birthdate'=>$birthdate,
    ]);
  }

  public function testDelete()
  {
    $client = factory(Client::class)->create();
    $headers = $this->getHeaders();
    $this->delete('client/'.$client->id,[],$headers);
    $this->seeStatusCode(200);
  }

  public function testShow()
  {
    $client = factory(Client::class)->create();
    $headers = $this->getHeaders();
    $this->get('client/'.$client->id,$headers);
    $this->seeStatusCode(200);

    $this->seejson([
      'id'=>$client->id,
    ]);
  }
}

 ?>
