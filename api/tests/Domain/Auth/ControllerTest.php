<?php
  namespace Domain\Auth;

  use Domain\User\User;
  use Domain\Client\Client;
  /**
   *
   */
  class ControllerTest extends \TestCase
  {

    public function testLogin()
   {
     //sets
     $data=[
       'username'=>'emtudo8',
       'password'=>'emtudo123',
     ];

     $user=$data;
     $user['password']=bcrypt($user['password']);
     $user['email']='teste8@teste.com';
     factory(User::class)->create($user);

     $this->post('auth/login',$data);
     //Asserts
     $this->seeStatusCode(200);
     $this->seejson([
       'username'=>'emtudo8',
     ]);
   }

    public function testLogout()
   {
     //sets
     $data=[
       'username'=>'teste2',
       'password'=>'emtudo123',
     ];

     $user=$data;
     $user['password']=bcrypt($user['password']);
     $user['email']='teste2@teste.com';
     factory(User::class)->create($user);

     $this->post('auth/login',$data);
     //$this->dd();
     //Asserts
     $this->seeStatusCode(200);
     $this->seejson([
       'username'=>'teste2',
     ]);
     $this->post('auth/logout',[],$this->getHeaders());

     $this->seeStatusCode(200);

   }

    public function testLoginClient()
   {
     //sets
     $data=[
       'username'=>'emtudo3',
       'password'=>'emtudo123',
     ];

     $user=$data;
     $user['password']=bcrypt($user['password']);
     $user['email']='teste3@teste.com';
     factory(Client::class)->create($user);

     $this->post('clients/auth/login',$data);
     //$this->dd();
     //Asserts
     $this->seeStatusCode(200);
     $this->seejson([
       'username'=>'emtudo3',
     ]);
   }



    public function testLoginWithEmail()
    {
      //sets
      $data=[
        'username'=>'emtudo5',
        'password'=>'emtudo123',
      ];

      $user=[
        'username'=>'emtudo5',
        'password'=>bcrypt($data['password']),
        'email'=>'teste5@teste.com',
      ];

      factory(User::class)->create($user);

      $this->post('auth/login',$data);

      //dd($this->response->getContent());

      //Asserts
      $this->seeStatusCode(200);
      $this->seejson([
        'username'=>'emtudo5',
      ]);
    }

    public function testCantLogin()
    {
      //sets
      $data=[
        'username'=>uniqid(),
        'password'=>'teste',
      ];

      $this->post('auth/login',$data);

      //dd($this->response->getContent());

      //Asserts
      $this->seeStatusCode(401);

    }
  }



 ?>
