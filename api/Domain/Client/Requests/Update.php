<?php
  namespace Domain\Client\Requests;

  class Update extends Store{
    public function rules()
    {
      $id = $this->route('client');
      $rules = parent::rules();
      return array_merge($rules,[
        'cpf' =>'cpf|unique:clients,cpf,'.$id,
      ]);
    }
  }


 ?>
