<?php
  /**
   *
   */
  namespace Domain\Client;

  use Domain\Client\Requests\Store;
  use Domain\Client\Requests\Update;

  class Controller extends \Domain\Core\Http\Controller
  {
    public function index()
    {
      return Client::all();
    }
    public function store(Store $request)
    {
      $data = $request->all();
      $client = new Client;
      $client->fill($data);
      $client->save();

      return $client;
    }

    public function update(Update $request, int $clientid)
    {
      $client=Client::find($clientid);
      $client->fill($request->all());
      $client->save();
      return $client;
    }

    public function destroy(int $clientid)
    {
      //return Client::destroy($clientid);

      $delete = Client::where('id', $clientid)
      ->first()
      ->delete();
      if ($delete) {
        return'Cliente Deletado';
      }else {
        return'Não foi possível deletar o cliente';
      }
    }

    public function show(int $clientid)
    {
      return Client::find($clientid);
    }

  }

 ?>
