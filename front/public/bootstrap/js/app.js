'use strict';

  angular.module("app",['ui.router'])
    .config(['$stateProvider',function($stateProvider){
      $stateProvider.state('home',{
        url:'/home',
        templateUrl:'/views/domain/dashboard/index.html',
      })

      .state('client',{
        url:'/clientes',
        templateUrl:'/views/domain/client/index.html',
        controller:'ClientCtrl',
      })

      .state('client_create',{
        url:'/clientes/criar',
        templateUrl:'/views/domain/client/form.html',
        controller:'ClientCtrl',
      })

      .state('client_edit',{
        url:'/clientes/{id}/editar',
        templateUrl:'/views/domain/client/form.html',
        controller:'ClientCtrl',
        params:{
          edit:true
        }
      })

    }])
