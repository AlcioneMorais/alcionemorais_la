
'use strict';

angular.module("app")
	.controller("LoginCtrl",['$scope','Restfull',function($scope, Restfull){
		$scope.auth=function(login)
    {
			Restfull.login('auth/login',login).then(function(response){
				console.log('successo');
				console.log(response);
        window.localStorage.setItem('token',response.data.token);
        window.localStorage.setItem('user',JSON.stringify(response.data.user));
        location.href="/";
			},function(response){
				console.log('Error no Login');
				console.log(response);
			});
		}
	}]);
