'use strict';
  angular.module("app")
  .controller("HomeCtrl",['$scope','Restfull',function($scope, Restfull){
      var user=JSON.parse(window.localStorage.getItem('user'));
    if(user!=null){
        $scope.username=user.name;
    }

    $scope.logout=function(){
      Restfull.post('auth/logout',{}).then(function(response){
        window.localStorage.removeItem('tokem');
        window.localStorage.removeItem('user');
        location.href="/login.html";
      },function(response){
        alert('Erro no Logout !!!');
      });
    }
  }]);
