'use strict';
  angular.module("app")
  .controller("ClientCtrl",['$scope','Restfull','$state','$stateParams',function($scope, Restfull, $state, $stateParams){
    //Listar_clients
    $scope.clients=[];
    Restfull.get('client').then(function(response){
      $scope.clients=response.data;
    },function(response){
      alert('Erro ao listar os clientes!!');
    });

 //test carrega client id se for EDITAR
    if($stateParams.edit === true){
      Restfull.get('client/'+$stateParams.id).then(function(response)
      {
        $scope.client = response.data;
      },function(response){
        alert('Erro ao Listar os Dados para Editar');
      });
    }

    //salvar e atualizar clientes
    $scope.save = function(client)
    {
     Restfull.save('client',client).then(function(response){
        alert('Cliente Salvo!!');
     },function(response){
       alert('Erro ao Salvar o Cliente!!');
     });
    }

    //editar clientes
    $scope.edit = function(client)
    {
      $scope.client = client;
      $state.go('client_edit',{id:client.id});
    }

    //apagar clientes
    $scope.delete = function(client)
    {
      Restfull.delete('client/'+client.id).then(function(response){
        var index = $scope.clients.indexOf(client);
        $scope.clients.splice(index, 1);
        alert('Cliente Deletado!!');
      },function(response){
        alert('Erro ao Deletar o Cliente!!');
      });
    }

    var user=JSON.parse(window.localStorage.getItem('user'));
    if(user!=null){
        $scope.username=user.name;
    }
  }]);
